# mfcc-python
Mel Frequency Cepstral Coefficients - build MFCC API on python

# Description
Create REST API to extract Mel Frequency Cepstral Coefficients (MFCC) from audio file. API implemented on python and build using Flask on local server. Clients are able to get MFCC output values from audio files and parameters (frame length, frame shift and number of mel banks) uploaded using POST request to server.  

# How to Run
This project has been tested to run using Python v3.8.10. Please install compatible version of python on your machine before moving forward.

## Dependencies
Required libraries included in requirements.txt. You can install this using pip. First change the current working directory to ```mfcc-python``` and then run :

```
\mfcc-python> pip install -r requirements.txt
```
- scipy == 1.8.0
- flask == 2.1.1
- librosa == 0.9.1
- numpy == 1.21
- requests == 2.26.0

To initiate the server, run ```server.py```.The server will run on http://127.0.0.1:5000
```
\mfcc-python> python server.py

 * Serving Flask app 'server' (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000 (Press CTRL+C to quit)
127.0.0.1 - - [21/Apr/2022 19:57:00] "POST / HTTP/1.1" 200 -
```

### _NOTE:_  If you wish to run it on virtual environment, you can initiate it on the project working directory. 
```
\mfcc-python> python -m venv \path\to\mfcc-python\directory\on\your\machine
```
And then activate it before moving forward to install dependencies

On Windows
```
\mfcc-python> Scripts\activate
```

On Linux/MacOS
```
~/mfcc-python$ source bin/activate
```
 

# How to Request into API
Endpoint on ('/') will receive POST request. 

To get MFCC value response from API, users need to include audio files and json file on body as part of POST request

## Audio file
Users upload audio file (.wav) into body request as parts of the multipart/form-data. Please use 'file' as key and audio file (.wav) as value.

## JSON file
Users may upload json file (.json) into body request as parts of the multipart/form-data. Pleae use 'parameters' as key and JSON file as value.
Users are able to post parameters value (frame length (ms), frame shift (ms) and number of mel banks) as json file. For example :
```
{
  "frame_length":25,
  "frame_shift":10,
  "num_mel_bins":26
}
```
## Example : Request using python script
This script was used to test API request using another script on client. 

To process this : Open another terminal, change directory into the project folder and you can find ```client.py```. If you use virtual environtment, activate it first and then run the script
```
\mfcc-python>python client.py
```

You will get JSON formatted respond 
```
{"audio_sample.wav":"[[-1154.813232421875, 1098.047119140625, -409.96728515625, 64.57443237304688, -378.0357971191406, 180.73233032226562, -116.8297348022461, 126.26396179199219, -161.9350128173828, 114.66415405273438, -29.271366119384766, 132.44741821289062, -112.46929168701172, 9.693145751953125,....
```

You can upload multiple audio files per request, to do that, open ```cliet.py``` -> add another file from local directory to this list
```python
#Audio file to be sent
audio_file = ['sources/audio_sample.wav']
```

For parameters (frame length, frame shift and number of mel banks) inside json file, you only allowed to upload single json file, to change parameter value open ```cliet.py``` -> add another file from local directory to this list
```python
local_json = 'sources/parameters.json'
json_file = json.load(open(local_json))
```

All local files saved on ```sources``` folder inside project directory

## Example : Request using Postman Application
If you have Postman installed on your machine, you can initiate POST request into http://127.0.0.1:5000. Choose ```Body->form-data```. Put audio files as file format with key 'file' (you can upload multiple audio files as long as the key is the same) and put parameters inside json files using file format with key 'parameters' (you only allowed to upload one json file per request). 

![Postman](sources/postman.png)

## Respond Output
You can expect json format (content-type header 'application/json') after successful request to API server with format below :

```
{
   "fileName1" : "13xN1 matrix(string)",
   "fileName2" : "13xN2 matrix(string)",
   "fileName3" : "13xN3 matrix(string)",
   ...
 }
```

