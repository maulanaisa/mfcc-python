import json
import requests

FRAME_LENGTH = 25
FRAME_SHIFT = 10
NUM_MEL_BINS = 26

frame_length = FRAME_LENGTH
frame_shift = FRAME_SHIFT
num_mel_bins = NUM_MEL_BINS

local_json = 'sources/parameters.json'
json_file = json.load(open(local_json))

#Parameters to be sent
parameters = {'frame_length' : json_file['frame_length'],'frame_shift'  : json_file['frame_shift'],'num_mel_bins' :  json_file['num_mel_bins']}

#Audio file to be sent
audio_file = ['sources/audio_sample.wav']

url = "http://127.0.0.1:5000"

files = [ ('parameters', open(local_json,'rb'))]

for file in audio_file :
    files.append(('file',(open(file, 'rb'))))

r = requests.post(url, files=files)

print(str(r.content, 'utf-8'))