#Import Dependencies
import librosa
import numpy as np
import scipy
from flask import Flask
from flask import request, jsonify, json

app = Flask(__name__)

FRAME_LENGTH = 25
FRAME_SHIFT = 10
NUM_MEL_BINS = 26

#Create endpoint
@app.route('/',methods=['POST'])
def mfcc_pipeline() :
    output = {}

    #Default value used when client do not provide json parameter file
    if 'parameters' not in request.files :
        frame_length = FRAME_LENGTH
        frame_shift = FRAME_SHIFT
        num_mel_bins = NUM_MEL_BINS
    else :
        parameters = json.load(request.files['parameters'])
        if 'frame_length' in parameters.keys() :
            frame_length = parameters['frame_length']
        else :
            frame_length = FRAME_LENGTH
        if 'frame_shift' in parameters.keys() :
            frame_shift = parameters['frame_shift']
        else :
            frame_shift = FRAME_SHIFT
        if 'num_mel_bins' in parameters.keys() :
            num_mel_bins = parameters['num_mel_bins']
        else :
            num_mel_bins = NUM_MEL_BINS

    if 'file' not in request.files :    #No audio file found on post request
        resp = jsonify({'message' : 'No file part in the request'})
        resp.status_code = 400
        return resp
    files = request.files.getlist("file")

    for file in files:      #Process multiple files
        audio, fs = loadFile(file)
        spectogram = stft(audio, frame_length, frame_shift, fs)
        mel_spectogram = mel_filter_bank(spectogram, num_mel_bins, frame_length, fs)
        mfcc = dct(mel_spectogram)
        output[file.filename] = json.dumps(mfcc.tolist())
        
    return jsonify(output)    #Output data as json
        
#Load audio file
def loadFile(path) :    
    audio,fs = librosa.load(path,sr=16000)
    return audio,fs

#DFT process on audio frames, returns power spectogram
def stft(audio, frame_length, frame_shift,fs) :
    stft = librosa.stft(audio, n_fft = int(frame_length*fs/1000), hop_length = int(frame_shift*fs/1000), window='hamming')
    spectogram = np.abs(stft) ** 2
    return spectogram

#Apply Mel Frequency Filter Bank, returns log Mel spectogram 
def mel_filter_bank(spectogram, num_mel_bins, frame_length, fs) :
    mel_filter = librosa.filters.mel(n_fft=int(frame_length*fs/1000), sr=fs, n_mels=num_mel_bins)
    mel_spectogram = np.matmul(mel_filter,spectogram)
    return np.log10(mel_spectogram)

#Apply DCT, returns first 13 cepstral coefficients
def dct(mel_spectogram, dct_type=3) :
    return scipy.fft.dct(mel_spectogram,dct_type)[0:13]

#Run Server
if __name__ == '__main__':
    app.run()